﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Virtual_Override_Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            #region  call insert function

            //Presty prestyObj = new Presty();
            //string str=prestyObj.Insert();
            //Console.WriteLine(str);

            //Deepak deepakObj = new Deepak();
            //string str = deepakObj.Insert();
            //Console.WriteLine(str);

            #endregion


            #region  call calculation function

            //Presty prestyObj = new Presty();
            //int val = prestyObj.Calculation(3,3);
            //Console.WriteLine(val);

            //Deepak deepakObj = new Deepak();
            //int val1 = deepakObj.Calculation(3,3);
            //Console.WriteLine(val1);

            Diwik diwikObj = new Diwik();
            int val2=diwikObj.Calculation(3, 3);
            Console.WriteLine(val2);

            #endregion
        }
    }

    public class Presty  //multilevel inheritance example 
    {
        public virtual string Insert()
        {
            return "presty";
        }

        public virtual int Calculation(int val1,int val2)
        {
            return val1 + val2;
        }
    }

    public class Deepak:Presty
    {
        public override string Insert()
        {
            return "deepak";
            //return base.Insert();
        }

        public override int Calculation(int val1, int val2)
        {
            return base.Calculation(val1, val2)*5;
        }
    }

    public class Diwik : Deepak
    {
        public override string Insert()
        {
            return base.Insert();
        }

        public override int Calculation(int val1, int val2)
        {
            return base.Calculation(val1, val2)*2;
        }
    }

   
}
